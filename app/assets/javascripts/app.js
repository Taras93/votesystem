angular.module('voteSystemAngular', ['ui.router', 'templates', 'Devise', 'ui-notification'])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('home', {
    url: '/home',
    templateUrl: 'home/_home.html',
    controller: 'MainCtrl',
    resolve: {
      votePromise: ['votes', function(votes) {
        return votes.getAll();
      }]
    },
    onEnter: ['$state', 'Auth', function($state, Auth) {
      if(!Auth.isAuthenticated()) {
        $state.go('login');
      }
    }]
  })
  .state('votesCrate', {
    url: '/votes/create',
    templateUrl: 'votes/_create.html',
    controller: 'VotesCtrl'
  })
  .state('login', {
    url: '/login',
    templateUrl: 'auth/_login.html',
    controller: 'AuthCtrl',
    onEnter: ['$state', 'Auth', function($state, Auth) {
      Auth.currentUser().then(function () {
        $state.go('home');
      })
    }]
  })
  .state('register', {
    url: '/register',
    templateUrl: 'auth/_register.html',
    controller: 'AuthCtrl',
    onEnter: ['$state', 'Auth', function($state, Auth) {
      Auth.currentUser().then(function () {
        $state.go('home');
      })
    }]
  });
  $urlRouterProvider.otherwise('home');
}]);