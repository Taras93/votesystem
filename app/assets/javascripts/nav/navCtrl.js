angular.module('voteSystemAngular')
.controller('NavCtrl', ['$scope', '$state', 'Auth', 'Notification', function($scope, $state, Auth, Notification) {
  $scope.signedIn = Auth.isAuthenticated;
  $scope.logout = Auth.logout;
  
  Auth.currentUser().then(function(user) {
    $scope.user = user;
  });
  
  $scope.$on('devise:new-registration', function(e, user) {
    $scope.user = user;
    Notification.success('Registration successful');
  });
  
  $scope.$on('devise:login', function(e, user) {
    $scope.user = user;
    Notification.success('Login successful');
  });
  
  $scope.$on('devise:logout', function(e, user) {
    $scope.user = {};
    $state.go('login');
    Notification.success('Logout successful');
  });
}]);