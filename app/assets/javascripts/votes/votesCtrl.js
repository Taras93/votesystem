angular.module('voteSystemAngular')
.controller('VotesCtrl', ['$scope', 'votes', function($scope, votes) {
  $scope.body = [];
  $scope.inputCounter = 0;
  $scope.isOptionsMin = true;
  $scope.isOptionsMax = false;
  $scope.isOptionsMinToCreate = true;
  
  $scope.limitOptions = function(el) {
    $scope.isOptionsMin = $scope.inputCounter <= 0 ? true : false;
    $scope.isOptionsMax = $scope.inputCounter >= 10 ? true : false;
    $scope.isOptionsMinToCreate = $scope.inputCounter >= 2 ? false : true;
    
    if($scope.isOptionsMax) {
      el.find('.add-option-btn').addClass('disabled');
    } else {
      el.find('.add-option-btn').removeClass('disabled');
    }
    
    if($scope.isOptionsMin) {
      el.find('.remove-option-btn').addClass('disabled');
    } else {
      el.find('.remove-option-btn').removeClass('disabled');
    }
    
    if($scope.isOptionsMinToCreate) {
      $('.create-vote-btn').prop('disabled', true);
    } else {
      $('.create-vote-btn').prop('disabled', false);
    }
  };
  
  function getOptions(options) {
    var optionObj = [];

    for (var i = 0; i < options.length; i++) {
     optionObj.push({'body': options[i], upvotes: 0});
    }

    return optionObj;
  }
  
  $scope.createVote = function() {
    
    votes.create({
      vote: {
        title: $scope.title,
        total: 0,
        options_attributes: getOptions($scope.body)
      }
    });
    
    $scope.title = '';
    $scope.body = [];
    $scope.inputCounter = 0;
    
  };
}])
.directive('newOption', ['$compile', '$log', function ($compile, $log) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      element.find('.add-option-btn').bind('click', function () {
        var input = angular.element('<div class="form-group" data-new-option="' + scope.inputCounter + '">' +
                                    '<input class="form-control type="text" ng-model="body[' + scope.inputCounter + ']" placeholder="Option" required>' +
                                    '</div>');
        var compile = $compile(input)(scope);
        element.append(input);
        scope.inputCounter++;
        scope.limitOptions(element);
      });
      element.find('.remove-option-btn').bind('click', function () {
        var lastInput = scope.inputCounter - 1; 
        $('[data-new-option="' + lastInput + '"]').remove();
        scope.inputCounter--;
        scope.limitOptions(element);
      });
    }
  }
}]);