angular.module('voteSystemAngular')
.factory('votes', ['$http', '$rootScope', '$log', function($http, $rootScope, $log) {
  var o = {
    votes: [],
    userVotes: []
  };
  
  o.getAll = function() {
    return $http.get('/votes.json').success(function(data) {
      angular.copy(data, o.votes);
    });
  };
  
  o.getUserVotes = function() {
    return $http.get('/user_votes.json').success(function(data) {
      angular.copy(data, o.userVotes);
    });
  };
  
  o.create = function(vote) {
    return $http.post('/votes.json', vote).success(function(data) {
      o.votes.push(data);
    });
  };
  
  o.incrementTotal = function(vote) {
    return $http.put('/votes/' + vote.id + '/increment_total.json')
      .success(function(data){
        vote.total++;
      });
  };
  
  o.upvote = function(vote, option) {
    return $http.put('/votes/' + vote.id + '/options/'+ option.id + '/upvote.json')
      .success(function(data){
        option.upvotes++;
      });
  };
  
  o.setUserToVote = function(vote) {
    return $http.put('/votes/' + vote.id + '/set_user_to_vote.json')
      .success(function(data){
        $log.log(data);
      });
  };
  
  return o;
}]);