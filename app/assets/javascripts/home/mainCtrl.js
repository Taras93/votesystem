angular.module('voteSystemAngular')
.controller('MainCtrl', ['$scope', '$window', '$log', 'votes', function($scope, $window, $log, votes) {
  $scope.votes = votes.votes;
  $scope.userVotes = votes.userVotes;
  
  $log.log($scope.userVotes);
  
  $scope.upVote = function(vote, options, currentOption) {
    votes.incrementTotal(vote);
    votes.upvote(vote, options[currentOption]);
    
    votes.setUserToVote(vote);
    
  };
}]);