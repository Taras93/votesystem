class VotesController < ApplicationController
  def index
    if current_user
      current_user_voted_votes = UserVote.where(user_id: current_user.id).map(&:vote_id)
      votes =  Vote.where.not(id: current_user_voted_votes)
    else
      votes = Vote.all
    end

    respond_to do |format|
      format.json { render json: votes.to_json(:include => :options) }
    end
  end
  
  def create
    respond_with Vote.create(vote_params)
  end
  
  def show
    respond_with Vote.find(params[:id])
  end
  
  def increment_total
    vote = Vote.find(params[:id])
    vote.increment!(:total)
    
    respond_to do |format|
      format.json { render json: vote }
    end
  end
  
  def set_user_to_vote
    vote = Vote.find(params[:id])
    user_vote = UserVote.new(user_id: current_user.id, vote_id: vote.id)
    user_vote.save!
    
    respond_to do |format|
      format.json { render json: user_vote }
    end
  end
  
  private
  def vote_params
    params.require(:vote).permit(:title, :total, options_attributes: [:body, :upvotes])
  end
end
