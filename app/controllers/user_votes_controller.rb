class UserVotesController < ApplicationController
  def index 
    user_votes =  UserVote.all
    
    respond_to do |format|
      format.json { render json: user_votes }
    end
  end
end
