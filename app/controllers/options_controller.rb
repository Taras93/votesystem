class OptionsController < ApplicationController
  def create
    vote = Vote.find(params[:id])
    option = vote.options.create(options_params)
  end
  
  def upvote
    vote = Vote.find(params[:vote_id])
    option = vote.options.find(params[:id])
    option.increment!(:upvotes)
    
    respond_to do |format|
      format.json { render json: vote.to_json(:include => :options) }
    end
  end
  
  private
  def options_params
    params.require(:option).permit(:body, :upvotes)
  end
end
