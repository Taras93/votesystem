class Vote < ActiveRecord::Base
  has_many :options
  has_many :users, :through => :user_votes 
  
  accepts_nested_attributes_for :options, :allow_destroy => true
  
#  def as_json(options = {})
#    super(options.merge(include: :options))
#  end
end
