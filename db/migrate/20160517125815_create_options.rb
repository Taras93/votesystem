class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.string :body
      t.integer :count
      t.references :vote, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
