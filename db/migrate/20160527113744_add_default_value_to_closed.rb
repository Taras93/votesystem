class AddDefaultValueToClosed < ActiveRecord::Migration
  def up
    change_column :votes, :closed, :boolean, :default => false
  end

  def down
    change_column :votes, :closed, :boolean, :default => nil
  end
end
