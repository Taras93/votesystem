class AddTotalToVotes < ActiveRecord::Migration
  def change
    add_column :votes, :total, :integer
  end
end
